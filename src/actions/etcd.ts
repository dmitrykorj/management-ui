import { Action } from './types';
import * as HttpStatus from 'http-status-codes';
import EnvironmentDeploy, { EnvironmentDeployStatus } from '../services/environment_deploy';
import async from 'async';
import { Dispatch } from 'redux';

export const PUT_DATA_SUCCESS = 'PUT_DATA_SUCCESS';
export const PUT_DATA_FAILURE = 'FETCH_DATA_FAILURE';
export const CHECK_KEY_EXISTS = 'CHECK_KEY_EXISTS';
export const DEPLOY_STATUS = 'DEPLOY_STATUS';
export const DEPLOY_SUCCESS = 'DEPLOY_SUCCESS';
export const DEPLOY_FAILURE = 'DEPLOY_FAILURE';

const environmentDeploy = new EnvironmentDeploy();

export const putDataSuccess = (data: object): Action => ({
  type: PUT_DATA_SUCCESS,
  payload: data,
});

export const putDataFailure = (error: object): Action => ({
  type: PUT_DATA_FAILURE,
  payload: error,
});

export const checkKeyExists = (data: object): Action => ({
  type: CHECK_KEY_EXISTS,
  payload: data,
});

export const deployStatus = (data: object): Action => ({
  type: DEPLOY_STATUS,
  payload: data,
});

export const deploySuccess = (): Action => ({
  type: DEPLOY_SUCCESS,
});

export const deployFailure = (data: object): Action => ({
  type: DEPLOY_FAILURE,
  payload: data,
});

export const triggerBranchDeploy = (data: string): object => {
  return (dispatch: Dispatch): any => {
    environmentDeploy
      .putBranch(data)
      .then((res: any) => {
        res.status === HttpStatus.OK
          ? dispatch(putDataSuccess(res))
          : dispatch(putDataFailure(res.statusText));
      })
      .catch(err => {
        dispatch(putDataFailure(err));
      });
  };
};

export const checkBranchStatus = (data: string): object => {
  return (dispatch: Dispatch): any => {
    environmentDeploy
      .checkBranch(data)
      .then((res: any) => {
        res.status === HttpStatus.OK
          ? dispatch(checkKeyExists(res.body))
          : dispatch(putDataFailure(res.statusText));
      })
      .catch(err => {
        dispatch(putDataFailure(err));
      });
  };
};

export const startPolling = (branchName: string): object => {
  return async (dispatch: Dispatch): Promise<any> => {
    await async.retry(
      {
        times: 600,
        interval: 1000,
      },
      async cb => {
        try {
          await environmentDeploy.checkBranch(branchName).then((res: any) => {
            if (res.body.hasOwnProperty('kvs')) {
              const [branchData] = res.body.kvs;
              const status = JSON.parse(atob(branchData.value)).status;
              dispatch(deployStatus(status));
              if (status === EnvironmentDeployStatus.DONE) {
                return dispatch(deploySuccess());
              }
            }
            cb(undefined, null);
          });
        } catch (err) {
          dispatch(deployFailure(err));
          cb(err, null);
        }
      },
    );
  };
};
