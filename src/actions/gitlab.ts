import { Action } from './types';
import Gitlab from '../services/gitlab';

export const FETCH_PROJECTS_SUCCESS = 'FETCH_PROJECTS_SUCCESS';
export const FETCH_PROJECT_BRANCHES_SUCCESS = 'FETCH_PROJECT_BRANCHES_SUCCESS';
export const FETCH_DATA_FAILURE = 'FETCH_DATA_FAILURE';

const gitlabApi = new Gitlab();

export const fetchProjectSuccess = (data: object): Action => ({
  type: FETCH_PROJECTS_SUCCESS,
  payload: data,
});

export const fetchDataFailure = (error: any): Action => ({
  type: FETCH_DATA_FAILURE,
  payload: error,
});

export const fetchProjectBranchesSuccess = (data: object): Action => ({
  type: FETCH_PROJECT_BRANCHES_SUCCESS,
  payload: data,
});

export const fetchProjects = (): object => {
  return (dispatch: any): any => {
    gitlabApi
      .getProjects()
      .then((res: any) => {
        dispatch(fetchProjectSuccess(res));
      })
      .catch(err => {
        dispatch(fetchDataFailure(err));
      });
  };
};

export const fetchProjectBranches = (projectId: number | string): object => {
  return (dispatch: any): any => {
    gitlabApi
      .getProjectBranches(projectId)
      .then((res: any) => {
        dispatch(fetchProjectBranchesSuccess(res));
      })
      .catch(err => {
        dispatch(fetchDataFailure(err));
      });
  };
};
