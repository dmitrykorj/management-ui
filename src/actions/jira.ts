import JiraApi from '../services/jira';
import { Action } from './types';
import { Dispatch } from 'redux';

export const FETCH_JIRA_PROJECTS_SUCCESS = 'FETCH_JIRA_PROJECTS_SUCCESS';
export const FETCH_JIRA_SPRINT_ISSUES_SUCCESS = 'FETCH_JIRA_SPRINT_ISSUES_SUCCESS';
export const FETCH_JIRA_PROJECT_SPRINTS_SUCCESS = 'FETCH_PROJECT_SPRINTS_SUCCESS';
export const FETCH_DATA_FAILURE = 'FETCH_DATA_FAILURE';
export const JIRA_TABLE_ISSUES_TOGGLE_CHECKBOX = 'JIRA_TABLE_ISSUES_TOGGLE_CHECKBOX';

const jiraApi = new JiraApi();

export const fetchProjectSuccess = (data: object): Action => ({
  type: FETCH_JIRA_PROJECTS_SUCCESS,
  payload: data,
});

export const fetchSprintIssuesSuccess = (data: object): Action => ({
  type: FETCH_JIRA_SPRINT_ISSUES_SUCCESS,
  payload: data,
});

export const fetchProjectSprintsSuccess = (data: object): Action => ({
  type: FETCH_JIRA_PROJECT_SPRINTS_SUCCESS,
  payload: data,
});

export const fetchDataFailure = (error: any): Action => ({
  type: FETCH_DATA_FAILURE,
  payload: error,
});

export const toggleCheckBox = (isChecked: boolean, key: string): Action => ({
  type: JIRA_TABLE_ISSUES_TOGGLE_CHECKBOX,
  payload: {
    isChecked,
    key,
  },
});

export const fetchProjects = (): object => {
  return (dispatch: Dispatch): any => {
    jiraApi
      .getProjects()
      .then((res: any) => {
        dispatch(fetchProjectSuccess(res.body));
      })
      .catch(err => {
        dispatch(fetchDataFailure(err));
      });
  };
};

export const fetchProjectSprints = (projectName: string): object => {
  return (dispatch: Dispatch): any => {
    jiraApi
      .getProjectSprints(projectName)
      .then((res: any) => {
        dispatch(fetchProjectSprintsSuccess(res.body));
      })
      .catch(err => {
        dispatch(fetchDataFailure(err));
      });
  };
};

export const fetchSprintIssues = (sprintId: number): object => {
  return (dispatch: Dispatch): any => {
    jiraApi
      .getSprintIssues(sprintId)
      .then((res: any) => {
        dispatch(fetchSprintIssuesSuccess(res.body));
      })
      .catch(err => {
        dispatch(fetchDataFailure(err));
      });
  };
};
