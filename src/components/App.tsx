import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Header from './Header/Header';
import { Container } from 'react-bulma-components';
import Content from './Content/Content';

const App = (): any => {
  return (
    <BrowserRouter>
      <Header />
      <Container>
        <Content />
      </Container>
    </BrowserRouter>
  );
};

export default App;
