import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Soon from '../Soon/Soon';
import JiraIssues from '../JiraIssues';
import Unnamed from '../Environment';

export default class Content extends Component {
  render(): React.ReactElement {
    return (
      <div>
        <Switch>
          <Route path="/soon" component={Soon} />
          <Route path="/jira_issues" component={JiraIssues} />
          <Route path="/gitlab" component={Unnamed} />
        </Switch>
      </div>
    );
  }
}
