import { bindActionCreators, Dispatch } from 'redux';
import { fetchProjectBranches, fetchProjects } from '../../actions/gitlab';
import { checkBranchStatus, triggerBranchDeploy, startPolling } from '../../actions/etcd';
import { connect } from 'react-redux';
import React, { useEffect, useState } from 'react';
import _ from 'lodash';
import SelectSearchBar from '../JiraIssues/SelectSearchBar';
import { Button, Loader } from 'react-bulma-components';

const Environment = (props: any): any => {
  const {
    projects,
    projectBranches,
    keyExists,
    branchStatusData,
    successDeploy,
    stopPolling,
  } = props;

  const [projectName, setProjectName] = useState('');
  const [branchName, setBranchName] = useState('');
  const [startButtonDisabled, setStartButtonDisabled] = useState(false);
  const [deployStarted, setDeployStarted] = useState(false);

  useEffect(() => {
    props.fetchProjects();
  }, []);

  useEffect(() => {
    projectName && props.fetchProjectBranches(projects[projectName].id);
  }, [projectName]);

  useEffect(() => {
    setStartButtonDisabled(false);
    branchName && props.checkBranchStatus(branchName);
  }, [branchName]);

  useEffect(() => {
    setStartButtonDisabled(false);
    branchName && props.checkBranchStatus(branchName);
  }, [deployStarted]);

  const handleGoButton = () => {
    setStartButtonDisabled(true);
    props.triggerBranchDeploy(branchName);
    props.startPolling(branchName);
  };

  return _.isEmpty(projects) ? (
    <Loader />
  ) : (
    <div>
      <div className="select-panel dropdown">
        <SelectSearchBar
          label={'Select project'}
          valueSelected={setProjectName}
          data={projects}
        />
        {!_.isEmpty(projectBranches) ? (
          <SelectSearchBar
            label={'Select branch'}
            valueSelected={setBranchName}
            data={projectBranches}
          />
        ) : null}
        {branchName && _.isUndefined(keyExists) ? <Loader /> : null}
        {branchName && !_.isUndefined(keyExists) ? (
          keyExists ? (
            <Button color="danger">STOP</Button>
          ) : (
            <Button disabled={startButtonDisabled} color="primary" onClick={handleGoButton}>
              START
            </Button>
          )
        ) : null}
        {branchStatusData ? <div>{branchStatusData}</div> : null}
      </div>
    </div>
  );
};

const mapStateToProps = (state: any): any => {
  return {
    projects: state.gitlab.projects,
    projectBranches: state.gitlab.projectBranches,
    putDataStatus: state.etcd.putDataSuccess,
    keyExists: state.etcd.keyExists,
    branchStatusData: state.etcd.branchStatusData,
    successDeploy: state.etcd.successDeploy,
    stopPolling: state.etcd.stopPolling,
  };
};
const mapDispatchToProps = (dispatch: Dispatch): any =>
  bindActionCreators(
    {
      fetchProjects,
      fetchProjectBranches,
      triggerBranchDeploy,
      checkBranchStatus,
      startPolling,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(Environment);
