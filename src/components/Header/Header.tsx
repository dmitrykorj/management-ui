import React from 'react';
import '../App.scss';
import { Navbar, Container } from 'react-bulma-components';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const header = (): any => {
  return (
    <Navbar color="white">
      <Container>
        <Navbar.Brand>
          <Link className="navbar-item" to="/jira_issues">
            JIRA issues
          </Link>
        </Navbar.Brand>
        <Navbar.Menu id="navMenu">
          <Navbar.Container>
            <Link className="navbar-item" to="/gitlab">
              Gitlab Projects
            </Link>
            <Link className="navbar-item" to="/jira_issues">
              JIRA issues
            </Link>
            <Link className="navbar-item" to="/soon">
              Soon link
            </Link>
          </Navbar.Container>
        </Navbar.Menu>
      </Container>
    </Navbar>
  );
};

const mapStateToProps = (state: any): any => {
  return {};
};

export default connect(mapStateToProps)(header);
