import { Form, Table } from 'react-bulma-components';
import React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { toggleCheckBox } from '../../actions/jira';
import { connect, ConnectedProps } from 'react-redux';

const mapStateToProps = (state: any): any => {
  return {
    sprintIssues: state.jiraIssues.sprintIssues,
  };
};
const mapDispatchToProps = (dispatch: Dispatch): any =>
  bindActionCreators({ toggleCheckBox }, dispatch);
const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux;
const IssuesTable = (props: Props): any => {
  const { sprintIssues } = props;

  return (
    <Table>
      <tbody>
        {sprintIssues.map((item: any, index: number) => (
          <tr key={index}>
            <td>
              <Form.Checkbox
                key={index}
                onChange={(): void => props.toggleCheckBox(!item.isChecked, item.key)}
                checked={item.isChecked}
              />
            </td>
            <td>{item.key}</td>
            <td>{item.fields.summary}</td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default connector(IssuesTable);
