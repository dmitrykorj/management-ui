import React, { MutableRefObject } from 'react';
import '../App.scss';

interface PrintContentProps {
  data: Array<object>;
  ref: any;
  placeholder?: string;
}

class PrintContent extends React.Component<PrintContentProps> {
  render(): any {
    return (
      <div className="print-content">
        {this.props.data.map((item: any, index: number) => (
          <div key={index} className="print-card">
            <div className="print-card-content">
              <div className="print-card-header">
                <div className="print-card-header-item">
                  <span> {item.fields.assignee.displayName}</span>
                </div>
                <div className="print-card-header-item">
                  <span> {item.key}</span>
                </div>
                <div className="print-card-header-item">
                  <span> {item.fields.status.name}</span>
                </div>
              </div>
              <div className="print-card-summary">{item.fields.summary}</div>
              {item.fields.description ? (
                <div className="print-card-description">
                  {item.fields.description.length > 120
                    ? item.fields.description.slice(0, 120) + ' ...'
                    : item.fields.description}
                </div>
              ) : null}
              <div className="print-card-footer"></div>
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default PrintContent;
