import React, { useState, useEffect } from 'react';
import { Dropdown, Form, Icon } from 'react-bulma-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faTimes } from '@fortawesome/free-solid-svg-icons';

type Props = {
  valueSelected: any;
  placeholder?: string;
  label: string;
  data: Array<string>;
};

const SelectSearchBar = (props: Props): any => {
  const [searchValue, setSearchValue] = useState('');
  const [searchResult, setSearchResult] = useState<Array<string>>([]);
  const [label, setLabel] = useState(props.label ? props.label : 'Select value');

  const { data } = props;

  const handleSearchChange = (event: any): any => {
    setSearchValue(event.target.value);
  };

  const handleSelectChange = (value: any): any => {
    props.valueSelected && props.valueSelected(value);
    setLabel(value);
  };

  useEffect(() => {
    if (searchValue) {
      const results = Object.keys(data).filter((item: string) =>
        item.toLowerCase().includes(searchValue.toLowerCase()),
      );

      setSearchResult(results);
    }
  }, [searchValue]);

  const inputIcon = searchValue ? (
    <FontAwesomeIcon icon={faTimes} />
  ) : (
    <FontAwesomeIcon icon={faSearch} />
  );

  return (
    // @ts-ignore
    <Dropdown label={label} onChange={handleSelectChange}>
      <Form.Field>
        <Form.Control className="has-icons-left has-icons-right">
          <Form.Input
            onChange={handleSearchChange}
            value={searchValue}
            placeholder={props.placeholder}
          />
          <Icon className="is-medium is-right">{inputIcon}</Icon>
        </Form.Control>
      </Form.Field>
      {searchResult.map((item: string, index: number) => (
        <Dropdown.Item key={index} value={item}>
          {item}
        </Dropdown.Item>
      ))}
    </Dropdown>
  );
};

export default SelectSearchBar;
