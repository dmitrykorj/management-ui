import { connect } from 'react-redux';
import React, { useState } from 'react';
import { Dropdown, Loader } from 'react-bulma-components';
import _ from 'lodash';

type Sprint = {
  closed: boolean;
  key: string;
  name: string;
};

type Props = {
  label: string;
  valueSelected: Function;
  sprints?: Array<Sprint>;
};

const SprintSelect = (props: Props): any => {
  const [label, setLabel] = useState(props.label ? props.label : 'Select');

  const { sprints } = props;

  const handleSelectChange = (value: any): any => {
    props.valueSelected && props.valueSelected(value);
    setLabel(value.name);
  };

  return !_.isEmpty(sprints) ? (
    // @ts-ignore
    <Dropdown label={label} onChange={handleSelectChange}>
      {sprints !== undefined &&
        sprints
          .filter((item: { closed: boolean }) => !item.closed)
          .map((item: { closed: boolean; key: string; name: string }, index: number) => (
            <Dropdown.Item key={index} value={item}>
              {item.name}
            </Dropdown.Item>
          ))}
    </Dropdown>
  ) : (
    <Loader />
  );
};

const mapStateToProps = (state: any): any => {
  return {
    sprints: state.jiraIssues.projectSprints,
  };
};

export default connect(mapStateToProps)(SprintSelect);
