import { bindActionCreators, Dispatch } from 'redux';
import {
  fetchProjects,
  fetchProjectSprints,
  fetchSprintIssues,
  toggleCheckBox,
} from '../../actions/jira';
import { connect } from 'react-redux';
import React, { useEffect, useRef, useState } from 'react';
import _ from 'lodash';
import SelectSearchBar from './SelectSearchBar';
import { Loader, Button } from 'react-bulma-components';
import SprintSelect from './SprintSelect';
import ReactToPrint from 'react-to-print';
import PrintContent from './PrintContent';
import IssuesTable from './IssuesTable';

type ProjectFields = {
  summary: string;
};

type DataItem = {
  isChecked: boolean;
  key: string;
  fields: ProjectFields;
};

type Sprint = {
  id?: number;
};

const JiraIssues = (props: any): any => {
  const { jiraProjects, sprintIssues } = props;

  const [projectName, setProjectName] = useState('');
  const [sprint, setSprint] = useState<Sprint>({ id: undefined });

  useEffect(() => {
    props.fetchProjects();
  }, []);

  useEffect(() => {
    projectName && props.fetchProjectSprints(projectName);
  }, [projectName]);

  useEffect(() => {
    sprint.id && props.fetchSprintIssues(sprint.id);
  }, [sprint]);

  const toggleAllCheckBox = () => {};

  const componentRef = useRef(null);

  return _.isEmpty(jiraProjects) ? (
    <Loader />
  ) : (
    <div>
      <div className="select-panel dropdown">
        <SelectSearchBar
          label={'Select project'}
          valueSelected={setProjectName}
          data={jiraProjects}
        />
        {projectName ? (
          <SprintSelect label={'Select sprint'} valueSelected={setSprint} />
        ) : null}
      </div>
      {!_.isEmpty(sprintIssues) ? (
        <div>
          <div className="select-panel control-buttons">
            <Button color="info" onClick={toggleAllCheckBox}>
              Select all
            </Button>
            <ReactToPrint
              trigger={(): any => <Button color="primary">Switch print</Button>}
              content={(): any => componentRef.current}
            />
            <div style={{ display: 'none' }}>
              <PrintContent
                data={sprintIssues.filter((item: DataItem) => item.isChecked)}
                ref={componentRef}
              />
            </div>
          </div>
          <IssuesTable {...props} />
        </div>
      ) : null}
    </div>
  );
};

const mapStateToProps = (state: any): any => {
  return {
    jiraProjects: state.jiraIssues.projects,
    projectSprints: state.jiraIssues.projectSprints,
    sprintIssues: state.jiraIssues.sprintIssues,
  };
};
const mapDispatchToProps = (dispatch: Dispatch): any =>
  bindActionCreators(
    { fetchProjects, fetchProjectSprints, fetchSprintIssues, toggleCheckBox },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(JiraIssues);
