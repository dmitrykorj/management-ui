import React, { Component } from 'react';
import { Menu as BulmaMenu, Columns } from 'react-bulma-components';

export default class Menu extends Component {
  render(): React.ReactElement {
    return (
      <Columns.Column className="is-3">
        <BulmaMenu className="is-hidden-mobile">
          <BulmaMenu.List title="General">
            <BulmaMenu.List.Item>Dashboards</BulmaMenu.List.Item>
            <BulmaMenu.List.Item>Customer</BulmaMenu.List.Item>
          </BulmaMenu.List>
        </BulmaMenu>
      </Columns.Column>
    );
  }
}
