export const GITLAB_API_URL = process.env.GITLAB_API_URL || '';
export const GITLAB_API_TOKEN = process.env.GITLAB_API_TOKEN || '';
export const JIRA_API_URL = process.env.JIRA_API_URL || '';
export const ETCD_SERVICE_URL = process.env.ETCD_SERVICE_URL || '';
