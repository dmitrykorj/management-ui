import {
  PUT_DATA_FAILURE,
  PUT_DATA_SUCCESS,
  CHECK_KEY_EXISTS,
  DEPLOY_STATUS,
  DEPLOY_SUCCESS,
} from '../actions/etcd';
import { EnvironmentDeployStatus } from '../services/environment_deploy';

const initialState = {
  keyExists: undefined,
  failureReason: '',
  branchStatusData: '',
  successDeploy: false,
  stopPolling: false,
};

const reducer = (state = initialState, action: any): any => {
  switch (action.type) {
    case PUT_DATA_SUCCESS:
      return {
        ...state,
        keyExists: true,
      };

    case PUT_DATA_FAILURE:
      return {
        ...state,
        failureReason: action.payload,
      };

    case CHECK_KEY_EXISTS:
      return {
        ...state,
        keyExists: action.payload.hasOwnProperty('count'),
      };

    case DEPLOY_STATUS:
      return {
        ...state,
        branchStatusData: action.payload,
      };

    case DEPLOY_SUCCESS:
      return {
        ...state,
        deploySuccess: true,
      };

    default:
      return state;
  }
};
export default reducer;
