import {
  FETCH_DATA_FAILURE,
  FETCH_PROJECTS_SUCCESS,
  FETCH_PROJECT_BRANCHES_SUCCESS,
} from '../actions/gitlab';

const initialState = {
  projects: [],
  projectBranches: [],
  error: null,
};

const reducer = (state = initialState, action: any): any => {
  switch (action.type) {
    case FETCH_PROJECTS_SUCCESS:
      const projectsData: any = {};
      action.payload.map((item: { name: string }) => {
        projectsData[item.name] = item;
      });
      return {
        ...state,
        projects: projectsData,
      };

    case FETCH_DATA_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        projects: [],
      };
    case FETCH_PROJECT_BRANCHES_SUCCESS:
      const projectBranchesData: any = {};
      action.payload.map((item: { name: string }) => {
        projectBranchesData[item.name] = item;
      });
      return {
        ...state,
        projectBranches: projectBranchesData,
      };

    default:
      return state;
  }
};
export default reducer;
