import { combineReducers } from 'redux';
import jiraIssues from './printJiraIssues';
import gitlab from './gitlab';
import etcd from './etcd';

export const rootReducer = combineReducers({
  jiraIssues,
  gitlab,
  etcd,
});

export default rootReducer;
