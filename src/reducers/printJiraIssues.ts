import {
  FETCH_DATA_FAILURE,
  FETCH_JIRA_PROJECTS_SUCCESS,
  FETCH_JIRA_PROJECT_SPRINTS_SUCCESS,
  FETCH_JIRA_SPRINT_ISSUES_SUCCESS,
  JIRA_TABLE_ISSUES_TOGGLE_CHECKBOX,
} from '../actions/jira';
import _ from 'lodash';

const initialState = {
  projects: [],
  projectSprints: [],
  sprintIssues: [],
  error: null,
};

const reducer = (state = initialState, action: any): any => {
  switch (action.type) {
    case FETCH_JIRA_PROJECTS_SUCCESS:
      const projectsData: any = {};
      action.payload.map((item: { key: string }) => {
        projectsData[item.key] = item;
      });
      return {
        ...state,
        projects: projectsData,
      };

    case JIRA_TABLE_ISSUES_TOGGLE_CHECKBOX:
      return {
        ...state,
        sprintIssues: state.sprintIssues.map((item: { key: string; isChecked: boolean }) => {
          if (item.key === action.payload.key) {
            item.isChecked = action.payload.isChecked;
          }
          return item;
        }),
      };

    case FETCH_JIRA_SPRINT_ISSUES_SUCCESS:
      const projectIssues: Array<object> = [];
      _.mapValues(action.payload, (item: any) => {
        projectIssues.push({
          key: item.key,
          fields: item.fields,
          isChecked: true,
        });
      });
      return {
        ...state,
        sprintIssues: projectIssues,
      };

    case FETCH_JIRA_PROJECT_SPRINTS_SUCCESS:
      return {
        ...state,
        projectSprints: action.payload,
      };

    case FETCH_DATA_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        projects: [],
      };

    default:
      return state;
  }
};
export default reducer;
