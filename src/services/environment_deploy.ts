import { ETCD_SERVICE_URL } from '../constants/app';
import request from 'superagent';
import EtcdClient from './etcd';

export default class EnvironmentDeploy extends EtcdClient {
  KEY_PREFIX = 'DEPLOY/ENVIRONMENT/STATUS';
  START_STATUS = 'pending';
  async putBranch(branchName: string): Promise<object> {
    return this.put(
      this.modifyKeyName(branchName),
      `{"environment_id": "${branchName}", "status": "${this.START_STATUS}"}`,
    );
  }

  async checkBranch(branchName: string): Promise<object> {
    return this.range(`${this.modifyKeyName(branchName)}`);
  }

  private modifyKeyName(branchName: string): string {
    return `${this.KEY_PREFIX}/${branchName.replace('-', '/')}`;
  }
}

export enum EnvironmentDeployStatus {
  PENDING = 'pending',
  ASSIGNING = 'assigning',
  DEPLOYING = 'deploying',
  DONE = 'done',
}
