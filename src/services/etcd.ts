import { ETCD_SERVICE_URL } from '../constants/app';
import request from 'superagent';

type requestBody = {
  key: string;
  value?: string;
  range?: string;
};
export default class EtcdClient {
  REST_PATH = 'v3/';

  async put(key: string, value = ''): Promise<object> {
    const body: requestBody = {
      key: btoa(key),
      value: btoa(value),
    };
    return request.post(`${ETCD_SERVICE_URL}${this.REST_PATH}kv/put`).send(body);
  }

  async range(key: string, range = ''): Promise<object> {
    const body: requestBody = {
      key: btoa(key),
      range: btoa(range),
    };
    return request.post(`${ETCD_SERVICE_URL}${this.REST_PATH}kv/range`).send(body);
  }
}
