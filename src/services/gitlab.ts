import { Gitlab as GitlabApi } from 'gitlab';
import { GITLAB_API_TOKEN, GITLAB_API_URL } from '../constants/app';

export default class Gitlab {
  api: GitlabApi;

  constructor() {
    this.api = new GitlabApi({
      token: GITLAB_API_TOKEN,
      host: GITLAB_API_URL,
    });
  }

  async getProjects(): Promise<object> {
    return await this.api.Projects.all();
  }

  async getProjectBranches(projectId: string | number): Promise<object> {
    return await this.api.Branches.all(projectId);
  }
}
