import { JIRA_API_URL } from '../constants/app';
import request from 'superagent';

export default class JiraApi {
  REST_PATH = 'api/v1/jira/';

  async fetch(query: string): Promise<object> {
    return request.get(`${JIRA_API_URL}${this.REST_PATH}${query}`);
  }

  async search(query: string): Promise<object> {
    return await this.fetch(`search?query=${query}`);
  }

  async getSprintIssues(sprintId: number): Promise<object> {
    return await this.search(`sprint=${sprintId}`);
  }

  async getProjects(): Promise<object> {
    return await this.fetch('projects');
  }

  async getProjectSprints(projectName: string): Promise<object> {
    return await this.fetch(`sprint/search/?query=project=${projectName}`);
  }
}
